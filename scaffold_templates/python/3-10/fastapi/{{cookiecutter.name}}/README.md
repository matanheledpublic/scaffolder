# {{cookiecutter.name}} Repository 🚀

A lean scaffold to kickstart your FastAPI project using Python 3.10.

## 📁 Project Structure

```
.
├── src/
│   ├── app.py          // Your main FastAPI application.
│   └── ...             // Other modules and packages.
├── tests/              
│   ├── test_app.py     // Tests for your FastAPI application.
│   └── ...             // Other test modules.
├── Dockerfile          // Docker file for containerization (if you're using Docker).
├── requirements.txt    // Project dependencies.
└── README.md           // Overview and instructions.
```

## 🔧 Setup & Running

### 1. **Install Dependencies**

Make sure you have Python 3.10 installed. Install the required packages with:

```bash
pip install -r requirements.txt
```

### 2. **Run the Application**

Navigate to the `src/` directory:

```bash
uvicorn app:app --reload
```

Access the app at:

```plaintext
http://127.0.0.1:8000/
```

### 3. **Interactive API Documentation**

Once the app is running, view the interactive API documentation:

```plaintext
http://127.0.0.1:8000/docs
```

## 🧪 Testing

Run tests using:

```bash
pytest tests/
```

## 🐳 Docker (Optional)

### 1. **Build Docker Image**

```bash
docker build -t fastapi-scaffold .
```

### 2. **Run in Docker**

```bash
docker run -p 8000:8000 fastapi-scaffold
```

Then, access the app at `http://127.0.0.1:8000/`.


from cookiecutter.main import cookiecutter
import os
import yaml
import shutil

def scaffold_microservice(template_path, microservices_path, cookiecutter_config):
    cookiecutter(
        template_path,
        output_dir=f"{microservices_path}",
        extra_context=cookiecutter_config,
        no_input=True
    )
    delete_resources_for_disabled_features(
        f"{microservices_path}/{cookiecutter_config['name']}/features.yaml",
        f"{microservices_path}/{cookiecutter_config['name']}"
    )

def delete_resources_for_disabled_features(feature_manifest_path, base_path):
    with open(feature_manifest_path) as manifest_file:
        manifest = yaml.full_load(manifest_file)
        for feature in manifest['features']:
            if not feature['enabled']:
                print("Removing resources for disabled feature {}...".format(feature['name']))
                for resource in feature['resources']:
                    delete_resource(f"{base_path}/{resource}")
    print("Cleanup complete, removing features.yaml manifest...")
    delete_resource(feature_manifest_path)


def delete_resource(resource):
    if os.path.isfile(resource):
        print("Removing file: {}".format(resource))
        os.remove(resource)
    elif os.path.isdir(resource):
        print ("Removing directory: {}".format(resource))
        shutil.rmtree(resource)
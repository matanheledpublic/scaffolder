import os
import shutil
from clients.scaffold import scaffold_microservice
from utils import capitalize_slug_string
import json

from consts import requiredEnvVars  # Assuming you have a consts.py with requiredEnvVars defined


# Validations
try:
	# Make sure all required env vars exist
	for envVar in requiredEnvVars:
		if envVar not in os.environ:
			raise ValueError(f"Missing environment variable: {envVar}")
	
	print("Made sure all env vars exist")
	# Inputs
	scaffold_dest_dir = os.environ.get('SCAFFOLD_DEST_DIR', 'scaffold_out')
	microservice_name = os.environ.get('SERVICE_NAME')
	template_language = os.environ.get('TEMPLATE_LANGUAGE')
	template_version = os.environ.get('TEMPLATE_VERSION')
	template_core = os.environ.get('TEMPLATE_CORE')
	template_features = json.loads(os.environ.get('TEMPLATE_FEATURES','[]'))

	print("Creating path variables")
	# Paths
	microservice_path = os.path.join(os.path.dirname(__file__), '..',  scaffold_dest_dir)
	new_microservice_path = os.path.join(microservice_path, microservice_name)  
	template_path = os.path.join(os.path.dirname(__file__), '..', 'scaffold_templates', template_language, template_version, template_core)
	
except Exception as e:
	print(f"Validations for scaffolding failed. Reason: {e}")
	exit(1)

cookiecutter_config = {
	"name": microservice_name
}
print(cookiecutter_config)

print("Starting scaffold")
scaffold_microservice(template_path, microservice_path, cookiecutter_config)







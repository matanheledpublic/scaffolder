#!/bin/bash

### SUMMARY OF INPUTS ###

# GITLAB_API_URL: The URL for the GitLab API, e.g., "https://gitlab.com/api/v4" for GitLab.com.
# GITLAB_ACCESS_TOKEN: Your GitLab Personal Access Token with appropriate permissions.
# GITLAB_GROUP_NAME: The namespace ID of the GitLab group where the repo should be created.
# SERVICE_NAME: The desired name for the new GitLab repository.
# SOURCECODE_PATH: The path to the source code directory to be pushed to the new repo (defaults to "scaffold_out/").

### INPUTS ###
GITLAB_API_URL="${GITLAB_API_URL:-https://gitlab.com/api/v4}"
GITLAB_ACCESS_TOKEN="${GITLAB_ACCESS_TOKEN}"
GITLAB_TOKEN_NAME="${GITLAB_TOKEN_NAME}"
GITLAB_GROUP_NAME="${GITLAB_GROUP_NAME}"
SERVICE_NAME="${SERVICE_NAME}"
SOURCECODE_PATH="${SOURCECODE_PATH:-scaffold_out/}"

# Ensure required environment variables are set
if [[ -z "$GITLAB_GROUP_NAME" ]] || [[ -z "$SERVICE_NAME" ]] || [[ -z "$GITLAB_ACCESS_TOKEN" ]]; then
    echo "Please ensure GITLAB_GROUP_NAME, SERVICE_NAME, and GITLAB_ACCESS_TOKEN environment variables are set."
    exit 1
fi

# Create new repository on GitLab
NAMESPACE_ID=$(curl -s --header "Private-Token: $GITLAB_ACCESS_TOKEN" "$GITLAB_API_URL/groups/$GITLAB_GROUP_NAME" | jq -r .id)
CREATE_REPO_RESPONSE=$(curl -s --header "Private-Token: $GITLAB_ACCESS_TOKEN" -X POST "$GITLAB_API_URL/projects" --form "name=$SERVICE_NAME" --form "namespace_id=$NAMESPACE_ID")
PROJECT_URL=$(echo $CREATE_REPO_RESPONSE | jq -r .http_url_to_repo)



# Check if the repository creation was successful
if [[ -z "$PROJECT_URL" ]]; then
    echo "Failed to create GitLab repository."
    exit 1
fi

# Navigate to the source code directory and push to the new repository
echo "Initializing new repository..."
cd $SOURCECODE_PATH/${SERVICE_NAME}
git init
git add .
git commit -m "Initial commit"
git remote add origin https://${GITLAB_TOKEN_NAME}:$GITLAB_ACCESS_TOKEN@gitlab.com/${GITLAB_GROUP_NAME}/${SERVICE_NAME}.git
git push -u origin main

echo "Source code pushed to $PROJECT_URL"

def capitalize_slug_string(string):
    words = string.split('-')
    # Capitalize the first letter of each word
    capitalized_words = [word.capitalize() for word in words]
    
    # Join the words with spaces
    result = ' '.join(capitalized_words)
    
    return result
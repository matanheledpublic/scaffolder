# Scaffolder

This directory contains a microservice Scaffolder project. Using parameterized Python code, you can scaffold a new microservice, and use the created files to create a new repository.
This is done using the `cookiecutter` Python library.

## Project hierarchy 
- `src` - This directory contains the Python code responsible for accepting parameters and scaffolding a service based on one of the templates
- `scaffold_templates` - This directory holds the different `cookiecutter` templates for our microservices. The directory hierarchy for `scaffold_templates` is as so:
```
scaffold_templates/
└── node
│   └── 18
│       └── fastify
│       │   ├── cookiecutter.json
│       │   └── {{cookiecutter.slug}}
│       │       ├── features.yaml
│       │       └── <TEMPLATED FILES>
│       └── vite
│           ├── cookiecutter.json
│           └── {{cookiecutter.slug}}
│               └── <TEMPLATED FILES>
└── golang
    └── 1-21
        └── gin
            ├── cookiecutter.json
            └── {{cookiecutter.slug}}
                ├── features.yaml
                └── <TEMPLATED FILES>
```
### Explanation
Under `scaffold_templates/` there are root directories for different languages (i.e. node, golang etc).
Under the language directories `scaffold_templates/{language}/`, there are directories for the language's versions (i.e. 18 - for node, or 1-21 - for golang).
Under the specific version directories `scaffold_templates/{language}/{version}/`, there are directories for different cores (for example fastify or vite for node, or gin for golang).

The core directories are those which hold the cookiecutter template itself. Each of these directories should have the following:
- `scaffold_templates/{language}/{version}/{core}/cookiecutter.json` - **File** - this file holds the configuration needed to be passed to the cookiecutter template, with the intended defaults. You can also use cookiecutter syntax to template parameters (look at `slug` & `ms_path` in the following example)
    ```json
    {
        "name": "Name of the service",
        "title": "Title of the service",
        "slug": "{{ cookiecutter.name.lower().replace(' ', '_') }}",
        "description": "A short description of the project",
        "features": {
            "kafka": false,
            "kafkaConsumer": false,
            "kafkaProducer": false,
            "mongo": false
        },
        "ms_path": "apps/{{ cookiecutter.slug }}"
    }
    ```
- `scaffold_templates/{language}/{version}/{core}/{{cookiecutter.slug}}/` - **Directory**- this directory holds the cookiecutter template itself. It should hold all of the base files for the wanted template. These directories should include:
    - The root of the `{{cookiecutter.slug}}` directory should hold the following:
        - `README.md` - a small readme defining the microservice.
        - `requirements` files for the microservice (`package.json`, `requirements.txt` etc)
        - Any other default files such as `.eslintrc.json` or `jest.config.ts`...
        - `features.yaml` - **OPTIONAL** - You can choose to add feature flags to your cookiecutter template. This file holds the configuration of the different feature flags your template has. This is an example, but a further explanation will be provided later:
        ```yaml
        features:
            - name: feature-name
                enabled: {{cookiecutter.features.feature-name|lower}}
                resources:
                    - "path/to/file/Feature1a.txt"
                    - "path/to/file/Feature1b.txt"
        ``` 
    - `src/` - the directory containing the templated source code. This directory should have all the basic, templated implementation of the code, core clients, core errors, core bootstraps and loaders etc. 



## Working with feature flags
Cookiecutter supports Jinja2 templates. This can be useful you want to add feature flags using conditions. [Here](https://cookiecutter.readthedocs.io/en/stable/advanced/boolean_variables.html#basic-usage) is a basic example from cookiecutter's official documentation.

To add a new feature flag you must:
1. Add the new feature `new-feature` to the `cookiecutter.json` file like so:
```json
{
    "name": "Name of the service",
    "title": "Title of the service",
    "slug": "{{ cookiecutter.name.lower().replace(' ', '_') }}",
    "description": "A short description of the project",
    "features": {
        "old-feature": false,
        "new-feature": false # new added feature name
    },
    "ms_path": "Full path to microservice"
}
```
2. Add the feature flag configuration to the `features.yaml` file inside the template directory (create it if it doesn't exist). It should look like this (the `resources` field will be explained bellow):
```yaml
features:
  - name: old-feature
    enabled: {{cookiecutter.features.old-feature|lower}}
    resources:
      - "path/to/file/Feature1a.txt"
      - "path/to/file/Feature1b.txt"

    # New feature configuration
  - name: new-feature
    enabled: {{cookiecutter.features.new-feature|lower}}
    resources:
      - "path/to/file/Feature2b.txt"
```

When adding feature flags to your code, there are 2 cases which need to be taken into account:
1. **In-file feature flags** - Lets say you have a `package.json` file, and you need to conditionally add the package `kafkajs` if the `kafka` feature flag is turned on. For this case, you will want to template the `package.json` conditionally. This is the syntax:
```yaml
...
	"dependencies": {
		"@aws-sdk/client-sqs": "^3.328.0",
		"@fastify/swagger": "^8.4.0",
		"@fastify/swagger-ui": "^1.8.1",
		"pino": "^8.11.0",
		{% if cookiecutter.features.kafka == true -%} # Checks if the variable .features.kafka was set to true
		"kafkajs": "^2.2.3"
		{% endif -%}
	},
...
```

2. **Entire file feature flags** - Cookiecutter doesn't provide a syntx for conditionally creating files. So, as part of the scaffolder Python script, some additional functionality is included:
Under the `features.yaml`, for each feature, there is a `resources` list field. This field should hold the list of files (relative to the template) which are only relevant to the specific feature flag.
If a file appears in a feature flag, but the feature flag is set to `false`, this file will NOT appear in the finalized template.

